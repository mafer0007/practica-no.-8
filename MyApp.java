package app;

import java.time.LocalDate;

import poo8.vehiculos;
import poo8.viajes;
import java.time.LocalDate;
import java.util.ArrayList;

public class myapp {

	public class MyApp {

		static void run() {

			viajes v = new viajes(2, "Korea", "Seul", LocalDate.of(2021, 11, 20), "kimchi", 8000.f);
			viajes d = new viajes(2, "Korea", "Seul", LocalDate.of(2021, 11, 20), "gimbap", 8000.f);

			System.out.println(v);
			System.out.println(d);

			System.out.println(v.equals(d));
			System.out.println(v.compareTo(d));
		}

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			run();

		}
	}

}
