package poo8;

import java.time.LocalDate;

public class viajes implements Comparable<viajes> {
	private int numeroViaje;
	private String ciudadDestino;
	private String direccion;
	private LocalDate fecharRegreso;
	private String descripcionCarga;
	private float montoViaje;

	public viajes(int numeroViaje, String ciudadDestino, String direccion, LocalDate fechaRregreso,
			String descripcionCarga, float montoViaje) {
		super();
		this.numeroViaje = numeroViaje;
		this.ciudadDestino = ciudadDestino;
		this.direccion = direccion;
		this.fecharRegreso = fechaRregreso;
		this.descripcionCarga = descripcionCarga;
		this.montoViaje = montoViaje;

	}

	public int getNumeroViaje() {
		return numeroViaje;
	}

	public void setNumeroViaje(int numeroViaje) {
		this.numeroViaje = numeroViaje;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public LocalDate getFecharRegreso() {
		return fecharRegreso;
	}

	public void setFecharRegreso(LocalDate fecharRegreso) {
		this.fecharRegreso = fecharRegreso;
	}

	public String getDescripcionCarga() {
		return descripcionCarga;
	}

	public void setDescripcionCarga(String descripcionCarga) {
		this.descripcionCarga = descripcionCarga;
	}

	public float getMontoViaje() {
		return montoViaje;
	}

	public void setMontoViaje(float montoViaje) {
		this.montoViaje = montoViaje;
	}

	public String getCiudadDestino() {
		return ciudadDestino;
	}

	@Override
	public String toString() {
		return "viajes [numeroViaje=" + numeroViaje + ", ciudadDestino=" + ciudadDestino + ", direccion=" + direccion
				+ ", fecharRegreso=" + fecharRegreso + ", descripcionCarga=" + descripcionCarga + ", montoViaje="
				+ montoViaje + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ciudadDestino == null) ? 0 : ciudadDestino.hashCode());
		result = prime * result + ((descripcionCarga == null) ? 0 : descripcionCarga.hashCode());
		result = prime * result + ((direccion == null) ? 0 : direccion.hashCode());
		result = prime * result + ((fecharRegreso == null) ? 0 : fecharRegreso.hashCode());
		result = prime * result + Float.floatToIntBits(montoViaje);
		result = prime * result + numeroViaje;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		viajes other = (viajes) obj;
		if (ciudadDestino == null) {
			if (other.ciudadDestino != null)
				return false;
		} else if (!ciudadDestino.equals(other.ciudadDestino))
			return false;
		if (descripcionCarga == null) {
			if (other.descripcionCarga != null)
				return false;
		} else if (!descripcionCarga.equals(other.descripcionCarga))
			return false;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		if (fecharRegreso == null) {
			if (other.fecharRegreso != null)
				return false;
		} else if (!fecharRegreso.equals(other.fecharRegreso))
			return false;
		if (Float.floatToIntBits(montoViaje) != Float.floatToIntBits(other.montoViaje))
			return false;
		if (numeroViaje != other.numeroViaje)
			return false;
		return true;
	}

	@Override
	public int compareTo(viajes v) {
		// TODO Auto-generated method stub
		if (!this.descripcionCarga.equals(v.getDescripcionCarga()))
			return this.descripcionCarga.compareTo(v.getDescripcionCarga());
		return 0;
	}
}
